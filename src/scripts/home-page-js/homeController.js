function homeController() {
    new IScroll('.home-content');
    new Swiper('.home-banner');
}

angular.module('myApp')
       .controller('homeController',homeController);