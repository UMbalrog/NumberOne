const express = require("express");
const ejs=require("ejs");
const fs = require("fs");
const mysql=require("mysql");
const body_parser = require("body-parser");

var app=express();

//数据库部分
/*var client=mysql.createConnection({
     host:"localhost",
     user:"root",
     password:"123",
     port:3306,
     database:"movie"
});
client.connect();
client.query("select * from movies",[50],function(err,reslt){
    console.log(reslt);

});*/

app.use(body_parser.urlencoded({extended:false})); //body_parser是接收post数据用的通过body
//res.body
app.use(express.static("./"));

app.set("views",__dirname+"/src/views/");
app.engine('html', ejs.__express);
app.set('view engine', 'html');


app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1');
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});


app.get("/",function(req,res,next){
    res.render("index");
});


var server=app.listen(3002,function(){
    console.log("3002.......");
    console.log("有人访问了");
});

